from math import sqrt
import sys


def distance_eucl(point1, point2):
    """
    Calcule la distance euclidienne entre les deux points fournis en argument
    point1 et point2 sont des points de deux coordonnées fournis sous forme de tuple
    Retourne la distance
    """

    assert len(point1) == len(point2), "Les deux points doivent avoir les mêmes dimensions"

    dimension = len(point1)
    somme_carré = 0

    for i in range(dimension):
        somme_carré += (point1[i] - point2[i]) ** 2

    return sqrt(somme_carré)


def distance_manh(point1, point2):
    """
    Calcule la distance manhattan entre les deux points fournis en argument
    point1 et point2 sont des points de deux coordonnées fournis sous forme de tuple
    Retourne la distance
    """

    assert len(point1) == len(point2), "Les deux points doivent avoir les mêmes dimensions"

    dimension = len(point1)
    somme_abs = 0

    for i in range(dimension):
        somme_abs += abs(point1[i] - point2[i])

    return somme_abs


def tri_selon(liste, rang, reverse=False):
    """
    Trie la liste selon le rang indiqué
    liste est une liste de tuple
    rang est le rang de la valeur selon laquelle on doit trier le tableau
    Retourne la liste triée
    """
    return sorted(liste, key=lambda element: element[rang], reverse=reverse)


def knn(donnees, categories, inconnu, nb_voisins):
    """
    Algorithme des K plus proches voisins
    donnees reprend l'ensemble des données numériques des individus
    categories reprend la catégorie de chaque individu
    inconnu reprend les données de l'individu à chercher
    nb_voisins, le nombre voisins à étudier
    Retourne une liste reprenant les couples (espèces, occurences) parmi les nb_voisins triée par occurence
    """
    distances = []

    for i in range(len(donnees)):
        couple = (distance_eucl(donnees[i], inconnu), categories[i])
        distances.append(couple)

    distances = tri_selon(distances, 0)

    dico = {}

    for couple in distances[:nb_voisins]:
        if couple[1] in dico:
            dico[couple[1]] += 1
        else:
            dico[couple[1]] = 1

    retour = [(k, v) for k, v in dico.items()]

    return sorted(retour, key=lambda x: x[1], reverse=True)


if __name__ == "__main__":
    nb_voisins = 2
    if len(sys.argv) > 1:
        nb_voisins = int(sys.argv[1])

    # Import des données
    train_stats = []
    with open("train_stats.csv", "r") as f:
        f.readline()
        for ligne in f:
            datas = ligne.strip().split(",")
            row = tuple(float(d) for d in datas)
            train_stats.append(row)

    train_labels = []
    with open("train_labels.csv", "r") as f:
        for ligne in f:
            train_labels.append(ligne.strip())

    test_stats = []
    with open("test_stats.csv", "r") as f:
        f.readline()
        for ligne in f:
            datas = ligne.strip().split(",")
            row = tuple(float(d) for d in datas)
            test_stats.append(row)

    test_labels = []
    with open("test_labels.csv", "r") as f:
        for ligne in f:
            test_labels.append(ligne.strip())

    erreurs = 0

    for index, inconnu in enumerate(test_stats):
        resultat = knn(train_stats, train_labels, inconnu, nb_voisins)
        if resultat[0][0] != test_labels[index]:
            erreurs += 1

    print("Pour", nb_voisins, "voisins :", erreurs / len(test_stats))
