# Bloc 2 - N. Revéret

Vous trouverez dans ce dépôt les documents de N. Revéret du Bloc 2 du DU-ESNSI.

Il comporte deux dossiers :
1. Tri et Correction
2. K PLus Proches Voisins

Vous pouvez :
* télécharger l'ensemble des documents et éxécuter les _notebooks_ dans _Jupyter Notebook_ 
* les lancer en ligne en suivant le lien : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/nreveret%2Fuco_reveret_bloc_2/master). Dans cette seconde solution vous n'aurez pas de sauvegarde automatique, il faudra télécharger les fichiers depuis _Binder_ à la fin de votre session) 